package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"datastore"
)

type server struct {
	datastore *datastore.Datastore
}

type StoreMessage struct {
	Id, Payload string
}

type RetrieveMessage struct {
	Id, Key string
}

func (s server) store(w http.ResponseWriter, r *http.Request) {

	var body StoreMessage

	if r.Body == nil {
		http.Error(w, "No request body found", 500)
	}

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	response, err := s.datastore.Store([]byte(body.Id), []byte(body.Payload))
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	if len(response) > 0 {
		fmt.Fprintf(w, "%s\n", string(response))
	} else {
		fmt.Fprintf(w, "No response\n")
	}
}

func (s server) retrieve(w http.ResponseWriter, r *http.Request) {

	var body RetrieveMessage

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	response, err := s.datastore.Retrieve([]byte(body.Id), []byte(body.Key))
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	if len(response) > 0 {
		fmt.Fprintf(w, "%s\n", string(response))
	} else {
		fmt.Fprintf(w, "No response\n")
	}

}

func main() {

	d, err := datastore.NewDatastore()
	if err != nil {
		panic(err)
	}

	s := server{d}

	http.HandleFunc("/store", s.store)
	http.HandleFunc("/retrieve", s.retrieve)

	log.Fatal(http.ListenAndServe(":8081", nil))

}
