package datastore

import (
	"aescrypto"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

// Datastore should have a mutex to stop data corruption issue
// the sql library may already implement this...
type Datastore struct {
	filename string
	db       *sql.DB
}

func NewDatastore() (d *Datastore, err error) {

	sql_table := `
    CREATE TABLE IF NOT EXISTS data(
        Id TEXT NOT NULL PRIMARY KEY,
        Data TEXT
    );

    `

	d = &Datastore{
		filename: "./datastore.db",
	}

	db, err := sql.Open("sqlite3", d.filename)
	if err != nil {
		return &Datastore{}, err
	}

	_, err = db.Exec(sql_table)

	d.db = db

	return d, err

}

// Store accepts an id and a payload in bytes and requests that the
// encryption-server stores them in its data store
func (d Datastore) Store(id, payload []byte) (aesKey []byte, err error) {

	aesKey, err = aescrypto.GenerateKey()
	if err != nil {
		return []byte(""), err
	}

	ciphertext, err := aescrypto.Encrypt(aesKey, payload)
	if err != nil {
		return []byte(""), err
	}

	cmd, err := d.db.Prepare("INSERT INTO data(Id, Data) values(?,?)")
	if err != nil {
		return []byte(""), err
	}

    // store the id with an implicit cast from []byte
	result, err := cmd.Exec(id, ciphertext)
	if err != nil {
		return []byte(""), err
	}

	_, err = result.LastInsertId()
	if err != nil {
		return []byte(""), err
	}

	return aesKey, nil
}

// Retrieve accepts an id and an AES key, and requests that the
// encryption-server retrieves the original (decrypted) bytes stored
// with the provided id
func (d Datastore) Retrieve(id, aesKey []byte) (payload []byte, err error) {

	row := d.db.QueryRow("SELECT Data FROM data WHERE Id=?", id)

	var ciphertext sql.NullString

	if err = row.Scan(&ciphertext); err != nil {
		return []byte(""), err
	}

	plaintext, err := aescrypto.Decrypt(aesKey, []byte(ciphertext.String))
	if err != nil {
		return []byte(""), err
	}

	return plaintext, nil
}
