This task was completed in the course of the recruiment process of the YOTI Core team.

# Dependancies
`go get github.com/mattn/go-sqlite3`

# To Build and Run

## Set your GOPATH varaiable:
`export GOPATH=/path/to/here`

## Build the server
`go build server`

## Install the server
`go install server`

## Run the server
`bin/server`

## Useful commands
### To store data
`curl -X POST localhost:8081/store -d '{"Id": "blah", "Payload": "hello"}'`

### To retrieve data
`curl -X POST localhost:8081/retrieve -d '{"Id": "blah", "Key": "InsertKeyHere"}'`